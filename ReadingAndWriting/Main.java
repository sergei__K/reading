package ReadingAndWriting;
/**
 * Created by g15oit18 on 12.12.2017.
 */
public class Main {
    public static void main(String[] args) {
        Flows flows1 = new Flows("src\\file1.txt","src\\result.txt");
        Flows flows2 = new Flows("src\\file2.txt","src\\result.txt");
        flows1.start();
        flows2.start();
    }
}
