package ReadingAndWriting;
import java.io.*;

/**
 * Created by g15oit18 on 12.12.2017.
 */
public class Flows extends Thread {
    private volatile String addres;
    private volatile String result;
    private volatile String  string;

    public Flows(String addres, String result) {
        this.addres = addres;
        this.result = result;
    }

    public void run() {

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(addres));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(result)))) {
            while ((string = bufferedReader.readLine()) != null) {
                try {
                    bufferedWriter.write(string);
                    try {
                        sleep(10);
                    } catch (InterruptedException e) {
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
